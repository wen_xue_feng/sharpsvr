using System;
using System.IO;

namespace sharpsvr.common
{
    public static class PathUtils
    {
        public static string GetCurrentProjectPath()
        {
            var path = Environment.CurrentDirectory;
            if (path.IndexOf("bin") > 0)
            {
                path = path.Substring(0, path.IndexOf("bin"));
            }
            return path;
        }

        public static string GetAppConfigFilePath(){
            return Path.Combine(GetCurrentProjectPath(), "app.config");
        }
    }
}